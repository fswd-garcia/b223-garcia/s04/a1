<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S04: Activity</title>
</head>
<body>
	<p><?php
	date_default_timezone_set('Asia/Manila');
	$updated_at = date("Y-m-d h:i:s");
	echo $updated_at;
	// header("Refresh: 2");
	?></p>


<h2>Building</h2>

<p>The name of the building is <?php echo $building->name; ?></p>

<p>The <?php echo $building->name; ?> Building has <?php echo $building->getFloor(); ?> floor </p>

<p>The <?php echo $building->name; ?> Building is located at <?php echo $building->getAddress(); ?></p>

<?php $building->setName('Caswyn Complex'); ?>

<p> The name of the building has been changed to <?php echo $building->getName(); ?></p>


<h2>Condominium</h2>

<p>The name of the condominium is <?php echo $condominium->name; ?></p>

<p>The <?php echo $condominium->name; ?> has <?php echo $condominium->getFloor(); ?> floor </p>

<p>The <?php echo $condominium->name; ?> is located at <?php echo $condominium->getAddress(); ?></p>

<?php $condominium->setName('Enzo Tower'); ?>

<p> The name of the condominium has been changed to <?php echo $condominium->getName(); ?></p>




	
</body>
</html>