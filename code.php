<?php  

class Building{

	public $name;
	protected $floors;
	protected $address; 

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address; 
	}

	public function getName(){
		return $this->name;
	}

	public function getFloor(){
		return $this->floors;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setName($name){
		// $this->name = $name;
		if(strlen($name) !== 0 ){
			$this->name = $name;
		}
	}

		
}

$building = new Building('Caswyn Building', 8,'Timog Avenue, Quezon City, Philippines');

class Condominium extends Building{


	
	

	}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Ave, Makati City Phils.');